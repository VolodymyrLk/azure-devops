$AzureDevOpsPAT = "e**************************************a"
$OrganizationName = "e******i"

$AzureDevOpsAuthenicationHeader = @{Authorization = 'Basic ' + [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(":$($AzureDevOpsPAT)")) }

$uriAccount = $UriOrga + "{project-name}/_apis/serviceendpoint/endpoints?api-version=5.1-preview.2"

Invoke-RestMethod `
  -Uri $uriAccount `
  -Method GET `
  -Headers $AzureDevOpsAuthenicationHeader

$uriAccount = $UriOrga + "{project-name}/_apis/serviceendpoint/endpoints/{service-endpoint-id}?api-version=5.1-preview.2"

Invoke-RestMethod `
  -Uri $uriAccount `
  -Method GET `
  -Headers $AzureDevOpsAuthenicationHeader
